#!/usr/bin/env python
"""
Name:         MASAS Activity Log Build
Author:       Jacob Westfall
Created:      Aug 25, 2012
Updated:      Oct 21, 2013
Copyright:    Independent Joint Copyright (c) 2012-2013 MASAS Contributors
License:      Published under the Modified BSD license.  See license.txt for 
              the full text of the license.
Description:  Performs a number of automated build operations.
"""

import os, sys
import optparse
import shutil
from subprocess import PIPE, Popen
try:
    from tools import mergejs
except:
    print "mergejs is required"
    sys.exit(1)
try:
    from tools import jsmin
except:
    print "jsmin is required"
    sys.exit(1)
try:
    from tools import cssmin
except:
    print "cssmin is required"
    sys.exit(1)
if not os.path.isfile("tools/yuicompressor-2.4.7.jar"):
    print "yuicompressor is required"
    sys.exit(1)


# set the relative destination directory, no trailing slash
DEST = "../deploy"  


def build_deploy_dirs(trial=False):
    """ Create any deploy directories needed by other build functions,
    should be run first """
    
    if trial:
        print "Nothing to do"
        return
    
    if not os.path.isdir(DEST):
        os.mkdir(DEST)
    # assumes a relative path and DEST will be added, must be in creation order
    dir_list = [\
        "libs",
        "js",
        "css",
        "libs/Bootstrap-2.3.2",
        "libs/Bootstrap-datetimepicker-0.0.11",
        "libs/OpenLayers-2.13.1",
    ]
    for each_dir in dir_list:
        new_dir = os.path.join(DEST, each_dir)
        if not os.path.isdir(new_dir):
            print "Creating directory %s" %new_dir
            os.mkdir(new_dir)
    
    print "\nDeployment directories ready"


def build_deploy_files(trial=False):
    """ Copy all files needed for deployment, should be run last """
    
    if trial:
        print "Nothing to do"
        return
    
    # assumes a relative path, specify a source file or directory and
    # destination directory, preceeding directory structure prior to copy will
    # be created, if a directory is specified all sub-dirs will also be copied
    #NOTE: renaming files during copy should also work
    file_list = [\
        ("../libs/Bootstrap-2.3.2/css/bootstrap.css",
            "libs/Bootstrap-2.3.2/css/bootstrap-debug.css"),
        ("../libs/Bootstrap-2.3.2/css/bootstrap.min.css",
            "libs/Bootstrap-2.3.2/css/bootstrap-min.css"),
        ("../libs/Bootstrap-2.3.2/js/bootstrap.js",
            "libs/Bootstrap-2.3.2/js/bootstrap-debug.js"),
        ("../libs/Bootstrap-2.3.2/js/bootstrap.min.js",
            "libs/Bootstrap-2.3.2/js/bootstrap-min.js"),
        ("../libs/Bootstrap-2.3.2/img/", "libs/Bootstrap-2.3.2/img/"),
        
        ("../libs/Bootstrap-datetimepicker-0.0.11/css/datetimepicker.min.css",
            "libs/Bootstrap-datetimepicker-0.0.11/css/datetimepicker-debug.css"),
        ("../libs/Bootstrap-datetimepicker-0.0.11/css/datetimepicker.min.css",
            "libs/Bootstrap-datetimepicker-0.0.11/css/datetimepicker-min.css"),
        ("../libs/Bootstrap-datetimepicker-0.0.11/js/datetimepicker.min.js",
            "libs/Bootstrap-datetimepicker-0.0.11/js/datetimepicker-debug.js"),
        ("../libs/Bootstrap-datetimepicker-0.0.11/js/datetimepicker.min.js",
            "libs/Bootstrap-datetimepicker-0.0.11/js/datetimepicker-min.js"),
        
        ("../libs/jquery-1.8.3.js", "libs/jquery-1.8.3-debug.js"),
        ("../libs/jquery-1.8.3.min.js", "libs/jquery-1.8.3-min.js"),
        
        ("../libs/OpenLayers-2.13.1/theme/", "libs/OpenLayers-2.13.1/theme/"),
        ("../libs/OpenLayers-2.13.1/img/", "libs/OpenLayers-2.13.1/img/"),
        
        ("../img/", "img/"),
    ]
    for source,destination in file_list:
        if not os.path.exists(source):
            print "Error: %s doesn't exist" %source
        destination = os.path.join(DEST, destination)
        if os.path.isdir(source):
            # need to remove any existing destination first for copytree
            if os.path.exists(destination):
                shutil.rmtree(destination)
            # let copytree create any directories needed
            shutil.copytree(source, destination,
                ignore=shutil.ignore_patterns("*.bak"))
        else:
            # create necessary directories when copying single files
            dest_dir = os.path.dirname(destination)
            if not os.path.exists(dest_dir):
                os.makedirs(dest_dir)
            shutil.copy(source, destination)
        print "Copied %s to %s" %(source, destination)
    
    print "\nDeployment files copied"


def build_package(name, source, config, output=None, merger=None, compressor=None,
                  type=None, debug=None, trial_run=False):
    """ Builds a package by merging and compressing a set of files """
    
    # defaults
    if not merger:
        merger = "mergejs"
    if not compressor:
        compressor = "jsmin"
    if not type:
        type = "js"
    
    print "\n-- Building %s Package --" %name
    print "Merging files with %s" %merger
    if merger == "simple":
        # config should instead be a list of filenames to merge
        #NOTE: no trial run available for this merger
        merge_result = []
        for each_file in config:
            file_path = os.path.join(source, each_file)
            print "Exporting: %s" %each_file
            file_header = "/* " + "=" * 70 + "\n   %s\n" %each_file + "   " + \
                "=" * 70 + " */\n\n"
            merge_result.append(file_header)
            with open(file_path) as file_data:
                merge_result.append(file_data.read())
                merge_result.append("\n")
        merged = "".join(merge_result)
    else:
        if trial_run:
            # used to list expected imports and dependencies based on
            # require statements in each javascript file header
            mergejs.run(source, None, config, True)
            print "-- Package Complete --"
            return
        else:
            merged = mergejs.run(source, None, config)
    if debug:
        print "Writing debug output to %s" %debug
        file(debug, "w").write(merged)
    if compressor == "jsmin":
        print "Compressing using jsmin"
        minimized = jsmin.jsmin(merged)
    elif compressor == "cssmin":
        print "Compressing using cssmin"
        minimized = cssmin.cssmin(merged)
    elif compressor == "yui":
        print "Compressing using YUI"
        proc = Popen("java -jar tools/yuicompressor-2.4.7.jar --type %s" %type,
            stdin=PIPE, stdout=PIPE, shell=True)
        minimized, err = proc.communicate(merged)
        if err:
            raise IOError("YUI Error: %s" %err)
    if output:
        print "Writing compressed output to %s" %output
        file(output, "w").write(minimized)
    
    print "-- Package Complete --"


# Packaging functions, prefix of build_ is required


def build_openlayers(trial=False):

    source_dir = "../libs/OpenLayers-2.13.1/lib"
    config_file = "openlayers.cfg"
    debug_file = "%s/libs/OpenLayers-2.13.1/OpenLayers-debug.js" %DEST
    compress_file = "%s/libs/OpenLayers-2.13.1/OpenLayers.js" %DEST

    build_package("OpenLayers", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_dateformat(trial=False):

    source_dir = "../libs"
    config_file = "dateformat.cfg"
    debug_file = "%s/libs/jquery.dateFormat-1.0-debug.js" %DEST
    compress_file = "%s/libs/jquery.dateFormat-1.0-min.js" %DEST

    build_package("DateFormat", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_handlebars(trial=False):

    source_dir = "../libs"
    config_file = "handlebars.cfg"
    debug_file = "%s/libs/handlebars-1.0.rc.1-debug.js" %DEST
    compress_file = "%s/libs/handlebars-1.0.rc.1-min.js" %DEST

    build_package("Handlebars", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_application_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return

    source_dir = "../css"
    config = ["activity.css"]
    debug_file = "%s/css/activity-debug.css" %DEST
    compress_file = "%s/css/activity-min.css" %DEST

    build_package("Application CSS", source_dir, config, output=compress_file,
        merger="simple", compressor="yui", type="css", debug=debug_file)


def build_application(trial=False):
    
    source_dir = "../js"
    config_file = "activity.cfg"
    debug_file = "%s/js/activity-debug.js" %DEST
    compress_file = "%s/js/activity-min.js" %DEST
    build_package("Application JS", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


if __name__ == '__main__':

    desc = """  These are the available modules that can be individually built:
    deploy_dirs, deploy_files, openlayers, dateformat, handlebars,
    application_css, application
    """
    parser = optparse.OptionParser(description=desc)
    parser.add_option("-d", help="set an alternate destination, relative or absolute",
        dest="new_dest", action="store")
    parser.add_option("-c", help="make clean by removing old deployment",
        dest="clean", default=False, action="store_true")
    parser.add_option("-p", help="build a single package",
        dest="package", action="store")
    parser.add_option("-t", help="trial run of a single package only",
        dest="trial", default=False, action="store_true")
    (opts, args) = parser.parse_args()
    
    if opts.new_dest:
        DEST = opts.new_dest
        print "Setting new destination to %s\n" %opts.new_dest
    if opts.clean:
        print "Removing old deployment\n"
        shutil.rmtree(DEST)
    if opts.trial and not opts.package:
        parser.error("You must specify a package for the trial run")
    if opts.package:
        mod = __import__(__name__)
        if not hasattr(mod, "build_%s" %opts.package):
            parser.error("Unable to find %s package" %opts.package)
        getattr(mod, "build_%s" %opts.package)(opts.trial)
    else:
        # these are in the correct build order
        build_deploy_dirs()
        build_openlayers()
        build_dateformat()
        build_handlebars()
        build_application_css()
        build_application()
        build_deploy_files()
