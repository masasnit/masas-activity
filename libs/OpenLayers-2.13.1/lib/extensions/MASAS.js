/* Original Work Copyright by OpenLayers Contributors and published under the
 * Clear BSD License.  See OpenLayers/license.txt for the full text of the license.
 *
 * Derivative Work Independent Joint Copyright (c) 2011-2012 MASAS Contributors.
 * Published under the Modified BSD license.  See license.txt for the full text of the license.
 *
 * This file contains the customization of the OpenLayers XML class to 
 * represent MASAS Atom feeds.  This includes parsing the feed and creating
 * appropriate OpenLayers geoemtry and feature objects.
 */

/**
 * @requires OpenLayers/Format/XML.js
 * @requires OpenLayers/Format/GML/v3.js
 * @requires OpenLayers/Feature/Vector.js
 */

/*global OpenLayers,VIEW */
 
/**
 * Class: OpenLayers.Format.MASASFeed
 * Read/write MASAS Atom feeds. Create a new instance with the
 *     <OpenLayers.Format.MASASFeed> constructor.
 *
 * Inherits from:
 *  - <OpenLayers.Format.XML>
 */
OpenLayers.Format.MASASFeed = OpenLayers.Class(OpenLayers.Format.XML, {
    
    /**
     * Property: namespaces
     * {Object} Mapping of namespace aliases to namespace URIs.  Properties
     *     of this object should not be set individually.  Read-only.  All
     *     XML subclasses should have their own namespaces object.  Use
     *     <setNamespace> to add or set a namespace alias after construction.
     */
    namespaces: {
        atom: "http://www.w3.org/2005/Atom",
        georss: "http://www.georss.org/georss",
        age: "http://purl.org/atompub/age/1.0",
        time: "masas:experimental:time"
    },
    
    /**
     * APIProperty: feedTitle
     * {String} Atom feed elements require a title.  Default is "untitled".
     */
    feedTitle: "untitled",

    /**
     * APIProperty: defaultEntryTitle
     * {String} Atom entry elements require a title.  In cases where one is
     *     not provided in the feature attributes, this will be used.  Default
     *     is "untitled".
     */
    defaultEntryTitle: "untitled",

    /**
     * Property: gmlParse
     * {Object} GML Format object for parsing features
     * Non-API and only created if necessary
     */
    gmlParser: null,
    
    /**
     * APIProperty: xy
     * {Boolean} Order of the GML coordinate: true:(x,y) or false:(y,x)
     * For GeoRSS the default is (y,x), therefore: false
     */
    xy: false,
    
    /**
     * APIProperty: circleNSides
     * {Integer} Number of sides for circle geometry regular polygon 
     */
    circleNSides: 40,
    
    /**
     * Constructor: OpenLayers.Format.AtomEntry
     * Create a new parser for Atom.
     *
     * Parameters:
     * options - {Object} An optional object whose properties will be set on
     *     this instance.
     */
    initialize: function (options) {
        OpenLayers.Format.XML.prototype.initialize.apply(this, [options]);
    },
    
    /**
     * APIMethod: read
     * Return a list of features from an Atom feed or entry document.
     
     * Parameters:
     * doc - {Element} or {String}
     *
     * Returns:
     * An Array of <OpenLayers.Feature.Vector>s
     */
    read: function (doc) {
        if (typeof doc == "string") {
            doc = OpenLayers.Format.XML.prototype.read.apply(this, [doc]);
        }
        
        try {
            // catching and ignoring errors because VIEW is not used by posting tool
            VIEW.map.baseLayer.events.register("moveend", this, this.checkGeometry);
        } catch (e) {} 
        return this.parseFeatures(doc);
    },

    /**
     * Method: getFirstChildValue
     *
     * Parameters:
     * node - {DOMElement}
     * nsuri - {String} Child node namespace uri ("*" for any).
     * name - {String} Child node name.
     * def - {String} Optional string default to return if no child found.
     *
     * Returns:
     * {String} The value of the first child with the given tag name.  Returns
     *     default value or empty string if none found.
     */
    getFirstChildValue: function (node, nsuri, name, def) {
        var value;
        var nodes = this.getElementsByTagNameNS(node, nsuri, name);
        if (nodes && nodes.length > 0) {
            value = this.getChildValue(nodes[0], def);
        } else {
            value = def;
        }
        return value;
    },

    /**
     * Method: parseFeature
     * Parse feature from an Atom entry node..
     *
     * Parameters:
     * node - {DOMElement} An Atom entry or feed node.
     *
     * Returns:
     * An <OpenLayers.Feature.Vector>.
     */
    parseFeature: function (node) {
        // some defaults
        var featureAttrib = {
            links: '',
            CAP: 'N',
            status: 'Actual',
            icon: OpenLayers.Format.MASASFeed.IconURL + 'ems/other/small.png',
            category: null,
            severity: '',
            certainty: ''
        };
        var atomns = this.namespaces.atom;
        var agens = this.namespaces.age;
        var timens = this.namespaces.time;
        var georssns = this.namespaces.georss;
        // variables used by this method
        var authors = null;
        var categories = null;
        var c_scheme = null;
        var links = null;
        var content_links = {};
        var l_rel = null;
        var l_type = null;
        var titles = null;
        var contents = null;
        var c_type = null;
        var xhtml_div = null;
        
        // atom Id
        featureAttrib.id = this.getFirstChildValue(node, atomns, "id", null);
        // atom Published
        featureAttrib.published = this.getFirstChildValue(node, atomns, "published", null);
        // atom Updated
        featureAttrib.updated = this.getFirstChildValue(node, atomns, "updated", null);
        // age Expires
        featureAttrib.expires = this.getFirstChildValue(node, agens, "expires", null);
        // time Effective
        featureAttrib.effective = this.getFirstChildValue(node, timens, "effective", null);
        // georss:point Text version
        //TODO: likely not needed anymore
        featureAttrib.point = this.getFirstChildValue(node, georssns, "point", null);
        
        // atom Author
        authors = this.getElementsByTagNameNS(node, atomns, "author");
        if (authors.length > 0) {
            //TODO: multiple authors?
            featureAttrib.author_name = this.getFirstChildValue(authors[0],
                atomns, "name", null);
            featureAttrib.author_uri = this.getFirstChildValue(authors[0],
                atomns, "uri", null);
        }
        
        // atom Category
        categories = this.getElementsByTagNameNS(node, atomns, "category");
        for (var i = 0; i < categories.length; i++) {
            try {
                c_scheme = categories[i].getAttribute('scheme').toLowerCase();
                if (c_scheme === 'masas:category:icon') {
                    featureAttrib.icon = OpenLayers.Format.MASASFeed.IconURL +
                        categories[i].getAttribute('term') + '/small.png';
                } else if (c_scheme === 'masas:category:status') {
                    featureAttrib.status = categories[i].getAttribute('term');
                } else if (c_scheme === 'masas:category:severity') {
                    featureAttrib.severity = categories[i].getAttribute('term');
                } else if (c_scheme === 'masas:category:certainty') {
                    featureAttrib.certainty = categories[i].getAttribute('term');
                } else if (c_scheme === 'masas:category:category') {
                    if (featureAttrib.category == null) {
                        featureAttrib.category = categories[i].getAttribute('term');
                    } else {
                        featureAttrib.category += ',' + categories[i].getAttribute('term');
                    }
                } else if (c_scheme === 'masas:category:colour') {
                    featureAttrib.colour = categories[i].getAttribute('term');
                }
            } catch (e) {}
        }
        
        // atom Links, string of space separated urls
        //TODO: perhaps change to array of links with content-types instead...
        links = this.getElementsByTagNameNS(node, atomns, "link");
        featureAttrib.links = {};
        for (var i = 0, linksObj = featureAttrib.links; i < links.length; i++) {
            try {
                var link = links[i];
                var linkObj = {
                    href: link.getAttribute('href'),
                    rel: link.getAttribute('rel'),
                    type: link.getAttribute('type') || "application/atom+xml",
                    hrefLang: link.getAttribute('hreflang'),
                    length: link.getAttribute('length')
                };
                switch (linkObj.rel) {
                case 'edit':
                    linksObj.Atom = linkObj;
                    break;
                case 'enclosure':
                    // quick short circuit for CAP 
                    if (linkObj.type === 'application/common-alerting-protocol+xml') {
                        linksObj.CAP = linkObj;
                        featureAttrib.CAP = 'Y';
                        // featureAttrib.enclosures.push({type:'cap',url:linkObj.href,mime:linkObj.type});
                    } else {
                        // look for known enclosure types
                        linksObj.attachments || (linksObj.attachments = []);
                        var known = false;
                        var title = links[i].getAttribute('title') ||
                            OpenLayers.Util.createUrlObject(linkObj.href).pathname.split('/').pop();
                        var obj = {
                            url: linkObj.href,
                            mime: linkObj.type,
                            'title': title,
                            length: linkObj.length,
                            hrefLang: linkObj.hrefLang
                        };
                        for (var type in OpenLayers.Format.MASASFeed.EnclosureRegEx) {
                            if (linkObj.type.search(OpenLayers.Format.MASASFeed.EnclosureRegEx[type]) >= 0) {
                                known = true;
                                obj.type = type;
                                linksObj.attachments.push(obj);
                                break;
                            }
                        }
                        if (!known) {
                            obj.type = "unknown";
                            linksObj.attachments.push(obj);
                        }
                    }
                    break;
                case 'related':
                    linksObj.xlink = linkObj;
                    break;
                case 'history':
                    linksObj.revisions = linkObj;
                    break;
                // use the rel attribute value as the links object key if
                // not a well known value & previously handled above
                default:
                    linksObj[linkObj.rel] = linkObj;
                }
            } catch (e) {}
        }
        
        // Atom <title>
        titles = this.getElementsByTagNameNS(node, atomns, 'title');
        if (titles.length > 0) {
            c_type = titles[0].getAttribute('type');
            if (c_type === 'text' || c_type == null) {
                // default for feeds where hub has done filtering because
                // lang=en or fr has been provided
                featureAttrib.title = this.getFirstChildValue(node, atomns,
                    'title', null);
            } else if (c_type === 'xhtml') {
                // support direct access to an entry where filtering has not
                // been done and title is nested in language divs
                //TODO: assumes first language div is english, support french
                xhtml_div = this.getChildEl(titles[0]);
                featureAttrib.title = this.getFirstChildValue(xhtml_div, '*',
                    'div', null);
            }
        }
        // Atom <content> - same parsing as title
        contents = this.getElementsByTagNameNS(node, atomns, 'content');
        if (contents.length > 0) {
            //TODO: is testing for "src" contents necessary?
            c_type = contents[0].getAttribute('type');
            if (c_type === 'text' || c_type == null) {
                featureAttrib.content = this.getFirstChildValue(node, atomns,
                    'content', null);
            } else if (c_type === 'xhtml') {
                xhtml_div = this.getChildEl(contents[0]);
                featureAttrib.content = this.getFirstChildValue(xhtml_div, '*',
                    'div', null);
            }
        }
        
        // atom Rights ?
        /* value = this.getFirstChildValue(node, atomns, "rights", null);
        if (value) {
            atomAttrib.rights = value;
        } */        
        // atom Summary ?
        /* value = this.getFirstChildValue(node, atomns, "summary", null);
        if (value) {
            atomAttrib.summary = value;
        } */
        
        // create the geometry for the Atom feature
        var geometry = this.parseLocations(node)[0];
        var originalGeom = geometry;
        if (geometry && geometry.CLASS_NAME !== 'OpenLayers.Geometry.Point') {
            var centroid = geometry.getCentroid();
            geometry = centroid;
            //featureAttrib.point = centroid.toShortString();
        }
        
        // create the OpenLayers Feature for this Atom entry
        var feature = new OpenLayers.Feature.Vector(geometry, featureAttrib);
        feature.fid = featureAttrib.id;
        feature.originalGeom = originalGeom;
        try {
            // catching and ignoring errors because VIEW is not used by posting tool
            feature.layer = VIEW.mapLayers.MASASLayer;
        } catch (e) {}
        feature.title = featureAttrib.title;  //or how to get attributes.title in a template?
        
        //console.log(feature);
        return feature;
    },
    
    /**
     * Method: checkGeometry
     * Resets the centroid geometry for non-point geometries and adjusts the 
     * point where the icon is displayed if it is partially off-screen.  Each
     * feature maintains an 'originalGeometry' property which holds the 
     * geometry as coontained in the feed.
     *
     * Parameters:
     * node - {DOMElement} An Atom entry or feed node.
     *
     * Returns:
     * An Array of <OpenLayers.Feature.Vector>s.
     */
    checkGeometry: function (ev) {
        if (!VIEW.adjustIcons && !ev.onLoad) {
            return;
        }
        
        var features = VIEW.mapLayers.MASASLayer.features;
        var mapExtent = VIEW.map.getExtent();
        var offset = VIEW.map.getResolution() * 100;
        //TODO: showDetailedPopups isn't available now, is this difference
        //      in offset even required?
        var xfactor = (VIEW.showDetailedPopups) ? 5 : 4;
        var yfactor = (VIEW.showDetailedPopups) ? 3 : 2;
        if (features) {
            // loop through all features
            for (var i = 0; i < features.length; ++i) {
                if (features[i].cluster) {
                    for (var j = 0, len = features[i].cluster.length; j < len; ++j) {
                        var geometry = features[i].cluster[j].originalGeom;
                        var centroid = geometry.getCentroid();
                        if (geometry && geometry.CLASS_NAME !== 'OpenLayers.Geometry.Point') {
                            var fBounds = geometry.getBounds();
                            
                            // case where the feature geometry completely overlaps the viewport
                            // move the icon point to a fixed position offset from the
                            // top-right corner
                            if (fBounds.containsBounds(mapExtent)) {
                                var newGeom = new OpenLayers.Geometry.Point(mapExtent.right - xfactor * offset, mapExtent.top - yfactor * offset);
                                if (ev.zoomChanged) {
                                    features[i].cluster[j].geometry = newGeom;
                                    features[i].cluster[j].adjustedCentroid = true;
                                } else {
                                    features[i].move(new OpenLayers.LonLat(newGeom.x, newGeom.y));
                                    features[i].adjustedCentroid = true;
                                    if (len === 1) {
                                        features[i].cluster[j].geometry = newGeom;
                                        features[i].cluster[j].adjustedCentroid = true;
                                    }
                                }
                                
                            }
                            // case where feature geometry just intersects the viewport
                            // move the icon point to the closest vertex that is within 
                            // the viewport
                            else if ((!mapExtent.contains(centroid.x, centroid.y)) && (mapExtent.intersectsBounds(fBounds))) {
                                var vertices = geometry.getVertices();
                                var dist = Number.MAX_VALUE;
                                var closest = -1;
                                var mapCenter = mapExtent.toGeometry().getCentroid();
                                for (var v = 0; v < vertices.length; ++v) {
                                    var testDist = vertices[v].distanceTo(mapCenter);
                                    if (testDist < dist) {
                                        dist = testDist;
                                        closest = v;
                                    }
                                }
                                if (closest > -1) {
                                    var newGeom = vertices[closest].clone();
                                    if (ev.zoomChanged) {
                                        features[i].cluster[j].geometry = newGeom;
                                        features[i].cluster[j].adjustedCentroid = true;
                                    } else {
                                        features[i].move(new OpenLayers.LonLat(newGeom.x, newGeom.y));
                                        features[i].adjustedCentroid = true;
                                        if (len == 1) {
                                            features[i].cluster[j].geometry = newGeom;
                                            features[i].cluster[j].adjustedCentroid = true;
                                        }
                                    }
                                }
                            }
                            // all other cases, just use the geometry as is
                            else {
                                var centroid = geometry.getCentroid();
                                if (features[i].cluster[j].adjustedCentroid) {
                                    //reset the geometry to default centroid 
                                    features[i].cluster[j].geometry = centroid;
                                    features[i].cluster[j].adjustedCentroid = false;
                                }
                                if (features[i].adjustedCentroid) {
                                    //reset the geometry to default centroid 
                                    features[i].move(new OpenLayers.LonLat(centroid.x, centroid.y));
                                    features[i].adjustedCentroid = false;
                                }
                            }
                        }
                    }//j loop
                } 
            }//i loop
        }
    },
    
    /**
     * Method: parseFeatures
     * Return features from an Atom entry or feed.
     *
     * Parameters:
     * node - {DOMElement} An Atom entry or feed node.
     *
     * Returns:
     * An Array of <OpenLayers.Feature.Vector>s.
     */
    parseFeatures: function (node) {
        var features = [];
        var entries = this.getElementsByTagNameNS(node, this.namespaces.atom, "entry");
        /* If there are no entries, don't parse the feed element itself
        if (entries.length == 0) {
            entries = [node];
        }
        */
        for (var i = 0, ii = entries.length; i < ii; i++) {
            features.push(this.parseFeature(entries[i]));
        }
        return features;
    },
    
    /**
     * Method: parseLocations
     * Parse the locations from an Atom entry or feed.
     *
     * Parameters:
     * node - {DOMElement} An Atom entry or feed node.
     *
     * Returns:
     * An Array of <OpenLayers.Geometry>s.
     */
    parseLocations: function (node) {
        var georssns = this.namespaces.georss;
        var i, ii;

        var locations = {components: []};
        var where = this.getElementsByTagNameNS(node, georssns, "where");
        if (where && where.length > 0) {
            if (!this.gmlParser) {
                this.initGmlParser();
            }
            for (i = 0, ii = where.length; i < ii; i++) {
                this.gmlParser.readChildNodes(where[i], locations);
            }
        }
        
        var components = locations.components;
        // point features
        var point = this.getElementsByTagNameNS(node, georssns, "point");
        if (point && point.length > 0) {
            for (i = 0, ii = point.length; i < ii; i++) {
                components.push(this.parseSimplePoint(point[i].firstChild.nodeValue));
            }
        }
        
        // line features
        var line = this.getElementsByTagNameNS(node, georssns, "line");
        if (line && line.length > 0) {
            for (i = 0, ii = line.length; i < ii; i++) {
                components.push(this.parseSimpleLine(line[i].firstChild.nodeValue));
            }
        }
        
        // polygon features
        var polygon = this.getElementsByTagNameNS(node, georssns, "polygon");
        if (polygon && polygon.length > 0) {
            for (i = 0, ii = polygon.length; i < ii; i++) {
                components.push(this.parseSimplePolygon(polygon[i].firstChild.nodeValue));
            }
        }
        
        // rectangular features
        var box = this.getElementsByTagNameNS(node, georssns, "box");
        if (box && box.length > 0) {
            for (i = 0, ii = box.length; i < ii; i++) {
                components.push(this.parseSimpleBox(box[i].firstChild.nodeValue));
            }
        }
        
        // convert to the projection of the map
        if (this.internalProjection && this.externalProjection) {
            for (i = 0, ii = components.length; i < ii; i++) {
                if (components[i]) {
                    components[i].transform(this.externalProjection, this.internalProjection);
                }
            }
        }
        
        // circle features
        // do circle after transform so that it is drawn in the map projection #4249
        var circle = this.getElementsByTagNameNS(node, georssns, "circle");
        if (circle && circle.length > 0) {
            for (i = 0, ii = circle.length; i < ii; i++) {
                if (this.internalProjection && this.externalProjection) {
                    components.push(this.parseSimpleCircle(
                        circle[i].firstChild.nodeValue, this.circleNSides,
                        this.internalProjection, this.externalProjection));
                } else {
                    components.push(this.parseSimpleCircle(
                        circle[i].firstChild.nodeValue, this.circleNSides));
                }
            }
        }
        
        return components;
    },
    
    /**
     * Simple GeoRSS point, parses XML text input from parseLocations or other
     * inputs as a general function.
     *
     * @param {string} text "lat lon"
     * @return {OpenLayers.Geometry.Point}
     */
    parseSimplePoint: function (text) {
        var xy = OpenLayers.String.trim(text).split(/\s+/);
        if (xy.length !== 2) {
            xy = OpenLayers.String.trim(text).split(/\s*,\s*/);
        }
        
        return new OpenLayers.Geometry.Point(parseFloat(xy[1]), parseFloat(xy[0]));
    },
    
    /**
     * Simple GeoRSS line
     *
     * @param {string} text "lat lon lat lon"
     * @return {OpenLayers.Geometry.LineString}
     */
    parseSimpleLine: function (text) {
        var coords = OpenLayers.String.trim(text).split(/\s+/);
        var points = [];
        var p;
        for (var i = 0, ii = coords.length; i < ii; i += 2) {
            p = new OpenLayers.Geometry.Point(parseFloat(coords[i + 1]),
                parseFloat(coords[i]));
            points.push(p);
        }
        
        return new OpenLayers.Geometry.LineString(points);
    },
    
    /**
     * Simple GeoRSS polygon
     *
     * @param {string} text "lat lon lat lon lat lon lat lon"
     * @return {OpenLayers.Geometry.Polygon}
     */
    parseSimplePolygon: function (text) {
        var coords = OpenLayers.String.trim(text).split(/\s+/);
        var points = [];
        var p;
        for (var i = 0, ii = coords.length; i < ii; i += 2) {
            p = new OpenLayers.Geometry.Point(parseFloat(coords[i + 1]),
                parseFloat(coords[i]));
            points.push(p);
        }
        
        return new OpenLayers.Geometry.Polygon(
            [new OpenLayers.Geometry.LinearRing(points)]);
    },
    
    /**
     * Simple GeoRSS box
     *
     * @param {string} text "lat lon lat lon"
     * @return {OpenLayers.Geometry.Polygon}
     */
    parseSimpleBox: function (text) {
        var coords = OpenLayers.String.trim(text).split(/\s+/);
        var points = [];
        points.push(new OpenLayers.Geometry.Point(  //lower-left
            parseFloat(coords[1]),
            parseFloat(coords[0])
        ));
        points.push(new OpenLayers.Geometry.Point(  //upper-left
            parseFloat(coords[1]),
            parseFloat(coords[2])
        ));
        points.push(new OpenLayers.Geometry.Point(  //upper-right
            parseFloat(coords[3]),
            parseFloat(coords[2])
        ));
        points.push(new OpenLayers.Geometry.Point(  //lower-right
            parseFloat(coords[3]),
            parseFloat(coords[0])
        ));
        points.push(new OpenLayers.Geometry.Point(  //repeat first point
            parseFloat(coords[1]),
            parseFloat(coords[0])
        ));
        
        return new OpenLayers.Geometry.Polygon(
            [new OpenLayers.Geometry.LinearRing(points)]);
    },
    
    /**
     * Simple GeoRSS circle
     *
     * @param {string} text "lat lon radius"
     * @return {OpenLayers.Geometry.Polygon}
     */
    parseSimpleCircle: function (text, sides, internalProj, externalProj) {
        var coords = OpenLayers.String.trim(text).split(/\s+/);
        if (!sides) {
            // default is 40 sides for a circular polygon
            sides = 40;
        }
        var origin = new OpenLayers.Geometry.Point(parseFloat(coords[1]),
            parseFloat(coords[0]));
        if (internalProj && externalProj) {
            origin = origin.transform(externalProj, internalProj);
        }
        // georss circle radius is in meters
        var radius = coords[2];
        //TODO: this .getUnits function only works if proj4js is loaded.
        //      A more reliable method will be to check the map object itself
        //      when this is added to the parsing options.
        /*
        var mapUnits = internalProj.getUnits();
        if (mapUnits != 'm') {   //convert meters to map units
          radius = radius / (OpenLayers.INCHES_PER_UNIT[mapUnits] * OpenLayers.METERS_PER_INCH);
        }
        */
        var circle = OpenLayers.Geometry.Polygon.createRegularPolygon(origin,
            radius, sides, 0);
        
        return circle;
    },
    
    CLASS_NAME: 'OpenLayers.Format.MASASFeed'
});

// trailing slash required
OpenLayers.Format.MASASFeed.IconURL = 'http://icon.masas-sics.ca/';

// regular expressions to decode the enclosure mime-types
OpenLayers.Format.MASASFeed.EnclosureRegEx = {
    image: /jpeg|gif|png|tiff|image/,
    video: /video/,
    text: /text\/plain/,
    spreadsheet: /text\/csv|excel/,
    word: /word|rtf/,
    pdf: /pdf/,
    kml: /kml\+xml/
};
