#!/usr/bin/env python
"""
Name:          Test Server
Author:        Jacob Westfall
Created:       Dec 19, 2012
Updated:       Dec 29, 2012
Copyright:     Independent Joint Copyright (c) 2011-2012 MASAS Contributors
License:       Published under the Modified BSD license.  See license.txt for 
                    the full text of the license.
Description:   This application serves html files locally and via a proxy.
"""

import os, sys
import urllib2
import cherrypy

# get absolute path name from executable   
server_pathname = os.path.dirname(sys.argv[0])
# change directory to absolute path name
os.chdir(os.path.abspath(server_pathname))


class RootPage(object):
    
    @cherrypy.expose
    def index(self):
        """ Create a recursive listing of all available web pages """
        
        html_files = []
        startDir = "."
        directories = [startDir]
        while len(directories) > 0:
            directory = directories.pop()
            for name in os.listdir(directory):
                fullpath = os.path.join(directory, name)
                if os.path.isfile(fullpath):
                    html_files.append(fullpath)
                elif os.path.isdir(fullpath):
                    directories.append(fullpath)
        
        html = ["<html>"]
        for new_file in html_files:
            html.append("""<a href="/%s"> %s </a>""" %(new_file, new_file))
            html.append("<br>")
        html.append("</html>")
        
        return str("\n".join(html))
    
    
    @cherrypy.expose
    def go(self, *args, **kwargs):
        """ Proxy for cross-domain javascript """
        
        if "url" not in kwargs:
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 3"
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 4"
        try:
            new_headers = {"User-Agent": "MASAS Viewing Tool Proxy"}
            if "Authorization" in cherrypy.request.headers:
                new_headers["Authorization"] = cherrypy.request.headers["Authorization"]
            if cherrypy.request.method == "POST":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.body.read()
                new_request = urllib2.Request(url, original_body, new_headers)
                # There is a bug in older python versions < 2.5 where only 200
                # is an acceptable response vs 201
                new_connection = urllib2.urlopen(new_request)
            elif cherrypy.request.method == "PUT":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.body.read()
                new_request = urllib2.Request(url, original_body, new_headers)
                # consider using httplib2 which has builtin PUT method instead
                new_request.get_method = lambda: 'PUT'
                new_connection = urllib2.urlopen(new_request)
            else:
                new_request = urllib2.Request(url, headers=new_headers)
                new_connection = urllib2.urlopen(new_request)
            new_info = new_connection.info()
            cherrypy.response.headers["Content-Type"] = \
                new_info.get("Content-Type", "text/plain")
            cherrypy.response.headers["Location"] = new_info.get("Location", None)
            # 10 MB max page size
            new_data = new_connection.read(10000000)
            new_connection.close()
            return new_data
        except Exception, err:
            cherrypy.response.status = 500
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nProxy Error:\n\n%s" %err



def run():
    """ Run method for standalone server"""
    
    server_config = {"server.socket_host": "0.0.0.0", "server.socket_port": 8080}
    app_config = {"/": {"tools.staticdir.root": os.path.abspath(server_pathname),
        "tools.staticdir.on": True, "tools.staticdir.dir": "."}}
    cherrypy.config.update(server_config)
    cherrypy.tree.mount(RootPage(), "/", app_config)
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == '__main__':
    run()
