/**
MASAS Activity Log - Activity Main
Updated: Oct 21, 2013
Independent Joint Copyright (c) 2012-2013 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

@requires feed.js
@requires entry.js
*/

/*global ACT,jQuery,$,OpenLayers,Handlebars,google */

/* TODO:
   - find problem with tooltips not always aligning to the left of the
     button when location maps are shown
   - consider Bootstrap responsive design features for better mobile support
   - Bootstrap 2.3.2 and OpenLayers 2.12 are the full versions not optimized
     to remove un-necessary components
*/

/**
Global Namespace
*/
//var ACT = window.ACT || {};


/**
Short names for common attachment types.  Can be extended by html page
to add custom names.
*/
ACT.ATTACH_TYPES = {
    'application/atom+xml': 'Atom',
    'application/common-alerting-protocol+xml': 'CAP',
    'image/jpeg': 'JPEG',
    'image/png': 'PNG',
    'application/vnd.google-earth.kml+xml': 'KML',
    'application/pdf': 'PDF Document',
    'application/vnd.ms-powerpoint': 'Powerpoint',
    'application/msword': 'Word',
    'text/plain': 'Text'
};


/* Utility Functions */

/**
Uses the browser's builtin parser for XML

@param {String} - the plain text version of some XML
@return {Object} - an XML DOM object
*/
ACT.parse_xml = function (xml) {
    var doc = null;
    if (window.DOMParser) {
        try {
            doc = (new DOMParser()).parseFromString(xml, 'text/xml');
        } catch (err) {
            doc = null;
            console.error('XML Parse Error: ' + err);
        }
    } else if (window.ActiveXObject) {
        try {
            doc = new ActiveXObject('Microsoft.XMLDOM');
            doc.async = false;
            if (!doc.loadXML(xml)) {
                doc = null;
                console.error('XML Parse Error: IE unable to parse');
            }
        } catch (err) {
            doc = null;
            console.error('XML Parse Error: ' + err);
        }
    }
    return doc;
};


/**
A custom sorting method that must be used with array .sort() for an array
of objects based on an object's property.

Reference: http://stackoverflow.com/questions/1129216/sorting-objects-in-an-array-by-a-field-value-in-javascript

@param {String} - the property value to use for sorting, using - as
a prefix reverses the sorting
@return {Integer} - the sort ordering for the object being compared
*/
ACT.array_sort = function (property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1, property.length - 1);
    }
    return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    };
};


/**
Removes an entry from an array

Reference: http://ejohn.org/blog/javascript-array-remove/

@param {Array} - the array to modify
@param {Integer} - the start location
@param {Integer} - the optional end location if removing multiple entries
@return {Array} - the newly modified array
*/
ACT.array_remove = function (array, from, to) {
    var rest = array.slice((to || from) + 1 || array.length);
    array.length = from < 0 ? array.length + from : from;
    return array.push.apply(array, rest);
};


/**
Parse an ISO formatted UTC datetime string into a timestamp.  Used
to accomodate lack of Date parsing capabilities in IE and Safari.

Reference: https://github.com/csnover/js-iso8601

@param {String} - the datetime string
@return {Integer} - the timestamp
*/
ACT.iso_utc_parse = function (date) {
    var timestamp, struct, minutesOffset = 0;
    var numericKeys = [ 1, 4, 5, 6, 7, 10, 11 ];
    // ES5 §15.9.4.2 states that the string should attempt to be parsed as a Date Time String Format string
    // before falling back to any implementation-specific date parsing, so that’s what we do, even if native
    // implementations could be faster
    // 1 YYYY 2 MM 3 DD 4 HH 5 mm 6 ss 7 msec 8 Z 9 ± 10 tzHH 11 tzmm
    if ((struct = /^(\d{4}|[+\-]\d{6})(?:-(\d{2})(?:-(\d{2}))?)?(?:T(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3}))?)?(?:(Z)|([+\-])(\d{2})(?::(\d{2}))?)?)?$/.exec(date))) {
        // avoid NaN timestamps caused by “undefined” values being passed to Date.UTC
        for (var i = 0, k; (k = numericKeys[i]); ++i) {
            struct[k] = +struct[k] || 0;
        }
        // allow undefined days and months
        struct[2] = (+struct[2] || 1) - 1;
        struct[3] = +struct[3] || 1;
        if (struct[8] !== 'Z' && struct[9] !== undefined) {
            minutesOffset = struct[10] * 60 + struct[11];
            
            if (struct[9] === '+') {
                minutesOffset = 0 - minutesOffset;
            }
        }
        timestamp = Date.UTC(struct[1], struct[2], struct[3], struct[4], struct[5] + minutesOffset, struct[6], struct[7]);
    } else {
        timestamp = Date.parse(date);
    }
    
    return timestamp;
};


/**
Output ISO formatted UTC datetime string suitable for Atom

@param {Object} - the Date object to use
@return {String} - a formatted datetime string
*/
ACT.iso_utc_format = function (date) {
    function pad(number) {
        var r = String(number);
        if (r.length === 1) {
            r = '0' + r;
        }
        return r;
    }
    
    return date.getUTCFullYear() +
        '-' + pad(date.getUTCMonth() + 1) +
        '-' + pad(date.getUTCDate()) +
        'T' + pad(date.getUTCHours()) +
        ':' + pad(date.getUTCMinutes()) +
        ':' + pad(date.getUTCSeconds()) + 'Z';  
};


/**
Expand and scroll a target entry into view, used for searching up
and down the entries list.

Reference: http://stackoverflow.com/questions/3432656/scroll-to-a-div-using-jquery

@param {Object} - the jQuery object to focus on
*/
ACT.goto_entry = function ($target) {
    var $details = $($target.find('.details')[0]);
    var $btn = $($target.find('.minMaxBtn i')[0]);
    // make sure the details are being shown
    if ($details.css('display') === 'none') {
        $details.css('display', '');
        $btn.removeClass('icon-plus-sign').addClass('icon-minus-sign');
    }
    // scrolls target to top, -50 accounts for top menu bar
    $('html,body').animate({scrollTop: $target.offset().top - 50}, 'slow');
};


/**
Fixes missing array.indexOf() in IE 8

Reference: https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/IndexOf
*/
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
        "use strict";
        if (this == null) {
            throw new TypeError();
        }
        var t = Object(this);
        var len = t.length >>> 0;
        if (len === 0) {
            return -1;
        }
        var n = 0;
        if (arguments.length > 1) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            } else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    };
}


/* Settings */

/**
Initializes the settings map used to select the area of interest when
the settings window is first opened, on subsequent openings it zooms
to the current area of interest.
*/
ACT.initialize_settings_map = function () {
    if (!ACT.settingsMap) {
        ACT.settingsMap = new OpenLayers.Map('settingsMap', {
            numZoomLevels: 19,
            projection: new OpenLayers.Projection('EPSG:900913'),
            displayProjection: new OpenLayers.Projection('EPSG:4326'),
            units: 'm',
            maxResolution: 'auto',
            maxExtent: new OpenLayers.Bounds(-20037508.34, -20037508.34, 20037508.34, 20037508.34),
            theme: null
        });
        
        // Override so that it zooms to the user's default view
        ACT.settingsMap.zoomToMaxExtent = ACT.zoom_to_default_view;
    
        var google_street = new OpenLayers.Layer.Google('Google Street', {
            type: google.maps.MapTypeId.ROADMAP,
            isBaseLayer: true
        });
        ACT.settingsMap.addLayer(google_street);
    }
    ACT.zoom_to_default_view();
};


/**
Zoom and re-center the map to either a default location (North America)
or to the user's saved location.
*/
ACT.zoom_to_default_view = function () {
    if (ACT.MAP_DEFAULT_VIEW) {
        var default_view_bounds = new OpenLayers.Bounds.fromString(ACT.MAP_DEFAULT_VIEW);
        default_view_bounds.transform(new OpenLayers.Projection('EPSG:4326'),
            ACT.settingsMap.getProjectionObject());
        ACT.settingsMap.zoomToExtent(default_view_bounds);
    } else {
        ACT.settingsMap.setCenter(new OpenLayers.LonLat(-94.0, 52.0).transform(
            new OpenLayers.Projection('EPSG:4326'), ACT.settingsMap.getProjectionObject()), 2);
    }
};


/**
Initializes the date and time selection plugin for settings.
*/
ACT.initialize_settings_datetime = function () {
    $('#datetimepicker1').datetimepicker({
      language: 'en',
      pickSeconds: false
    });
    $('#datetimepicker2').datetimepicker({
      language: 'en',
      pickSeconds: false
    });
    // dynamically show the pickers
    $('#inputAge').on('change', function () {
        var age_val = $('#inputAge').val();
        if (age_val === '999') {
            $('#dateRange').show();
            // disabling auto poll as its anticipated the user wants to review
            // past activity only
            $('#inputAuto').attr('checked', false);
        } else {
            $('#dateRange').hide();
            // resets date values if they weren't saved or used before
            if (!ACT.ENTRY_DATE_START) {
                $('#dateStart').val('');
                $('#dateStart').change();
            }
            if (!ACT.ENTRY_DATE_END) {
                $('#dateEnd').val('');
                $('#dateEnd').change();
            }
        }
    });
    $('#inputAge').change();
};


/**
Load the current settings when the settings window is opened
*/
ACT.load_current_settings = function () {
    if (ACT.USER_SECRET) {
        $('#inputSecret').val(ACT.USER_SECRET);
    }
    if (ACT.ENTRY_MAX) {
        $('#inputMax').val(ACT.ENTRY_MAX);
    }
    if (ACT.ENTRY_AGE) {
        $('#inputAge').val(ACT.ENTRY_AGE);
    }
    if (ACT.ENTRY_POLL) {
        $('#inputPoll').val(ACT.ENTRY_POLL);
    }
    if (ACT.ENTRY_AUTO_POLL !== 'undefined') {
        $('#inputAuto').attr('checked', ACT.ENTRY_AUTO_POLL);
    }
    if (ACT.ENTRY_HISTORY !== 'undefined') {
        $('#inputHistory').attr('checked', ACT.ENTRY_HISTORY);
    }
    ACT.initialize_settings_map();
    ACT.initialize_settings_datetime();
};


/**
Saves all selected settings when the window is closed.  Doesn't compare to
existing currently, simply writes again.
*/
ACT.save_new_settings = function () {
    console.debug('Saving new settings');
    // simple validation of the secret
    var new_secret = $('#inputSecret').val();
    if (!new_secret || new_secret.length < 6) {
        $('#inputSecretAlert').css('display', '');
        return;
    }
    ACT.USER_SECRET = new_secret;
    // re-hide in case it was previously shown
    $('#inputSecretAlert').css('display', 'none');
    // not validating these
    ACT.ENTRY_MAX = parseInt($('#inputMax').val(), 10);
    ACT.ENTRY_AGE = parseInt($('#inputAge').val(), 10);
    ACT.ENTRY_POLL = parseInt($('#inputPoll').val(), 10);
    // either is undefined or checked for a value
    var new_auto = $('#inputAuto').attr('checked');
    ACT.ENTRY_AUTO_POLL = (new_auto === 'checked') ? true : false;
    //TODO: may need an additional confirmation popup when a user sets
    //      this as true to make sure they do want all history
    var new_history = $('#inputHistory').attr('checked');
    ACT.ENTRY_HISTORY = (new_history === 'checked') ? true : false;
    // because of the aspect ratio of the map, if no "change" was made
    // to the default_view, the new view will still be somewhat different
    // when it is saved
    var view_bounds = ACT.settingsMap.getExtent().toArray();
    var new_view = new OpenLayers.Bounds.fromArray(view_bounds);
    new_view.transform(ACT.settingsMap.getProjectionObject(),
        new OpenLayers.Projection('EPSG:4326'));
    ACT.MAP_DEFAULT_VIEW = new_view.toBBOX(4);
    // either or reset these date values, using local time for input
    if (ACT.ENTRY_AGE === 999) {
        ACT.ENTRY_DATE_START = $('#datetimepicker1').data('datetimepicker').getLocalDate();
        ACT.ENTRY_DATE_END = $('#datetimepicker2').data('datetimepicker').getLocalDate();
    } else{
        ACT.ENTRY_DATE_START = null;
        ACT.ENTRY_DATE_END = null;
    }
    
    // restart the polling timer to accomodate any change in settings
    ACT.load_activity_feed_stop();
    ACT.load_activity_feed_start();
    
    $('#settingsModal').modal('hide');
};


/* Activity Map */

/**
Loads the activity map used to show an outline of where current activity
is taking place on a map.
*/
ACT.load_activity_map = function () {
    // initialize the map if it doesn't already exist
    if (!ACT.activityMap) {
        ACT.activityMap = new OpenLayers.Map('activityMap', {
            numZoomLevels: 19,
            projection: new OpenLayers.Projection('EPSG:900913'),
            displayProjection: new OpenLayers.Projection('EPSG:4326'),
            units: 'm',
            maxResolution: 'auto',
            maxExtent: new OpenLayers.Bounds(-20037508.34, -20037508.34, 20037508.34, 20037508.34),
            theme: null
        });
        // google base map for now
        var google_street = new OpenLayers.Layer.Google('Google Street', {
            type: google.maps.MapTypeId.ROADMAP,
            isBaseLayer: true
        });
        ACT.activityMap.addLayer(google_street);
        // feature layer displays using clusters to show active locations
        var cluster_style = new OpenLayers.Style({
            fillColor: '#0000CC',
            fillOpacity: 0.5,
            strokeColor: '#0000CC',
            strokeOpacity: 0.5,
            strokeWidth: 1,
            pointRadius: '${radius}',
            label: '${label}',
            labelSelect: true,
            fontWeight: 'bold',
            fontColor: '#FFFFFF',
            fontSize: 11,
            fontOpacity: 1
        }, {
            context: {
                radius: function (feature) {
                    var px = 5;
                    if (feature.cluster) {
                        px = 10;
                        if (feature.cluster.length > 10) {
                            px = 15;
                        } else if (feature.cluster.length > 50) {
                            px = 20;
                        }
                    }
                    return px;
                },
                label: function (feature) {
                    if (feature.cluster) {
                        return feature.cluster.length;
                    } else {
                        return '';
                    }
                }
            }
        });
        var style_map = new OpenLayers.StyleMap({"default": cluster_style,
            "select": cluster_style});
        ACT.activityLayer = new OpenLayers.Layer.Vector('Activity', {
            strategies: [new OpenLayers.Strategy.Cluster({distance: 15, threshold: 2})],
            isBaseLayer: false,
            projection: new OpenLayers.Projection('EPSG:4326'),
            styleMap: style_map
        });
        ACT.activityMap.addLayer(ACT.activityLayer);
        // start with a default view assuming no features yet
        ACT.activityMap.setCenter(new OpenLayers.LonLat(-94.0, 52.0).transform(
            new OpenLayers.Projection('EPSG:4326'), ACT.activityMap.getProjectionObject()), 2);
    }
    // update the layer with the latest Entry locations
    var activity_format = new OpenLayers.Format.GeoJSON({
        externalProjection: new OpenLayers.Projection('EPSG:4326'),
        internalProjection: new OpenLayers.Projection('EPSG:900913'),
        extractAttributes: true
    });
    ACT.activityLayer.removeAllFeatures();
    ACT.activityLayer.addFeatures(activity_format.read(ACT.feed.export_entry_locations()));
    var activity_extent = ACT.activityLayer.getDataExtent();
    if (activity_extent) {
        ACT.activityMap.zoomToExtent(activity_extent);
    }
};


/* Event Handlers */

/**
Toggles the Entry details expand/contract

@param {Object} - the jQuery event Object
*/
ACT.toggle_entry_details = function (evt) {
    // prevents the browser's default behaviour from going to href="#"
    evt.preventDefault();
    // $ is prefix for a jquery object
    var $btn = $(this);
    // finds the parent div to start from
    var $entry = $btn.closest('.entry');
    var $details = $($entry.find('.details')[0]);
    var $image = $($btn.find('i')[0]);
    if ($details.css('display') === 'none') {
        // faster than using hide when lots of dom elements
        $details.css('display', '');
        $image.removeClass('icon-plus-sign').addClass('icon-minus-sign');
    } else {
        $details.css('display', 'none');
        $image.removeClass('icon-minus-sign').addClass('icon-plus-sign');
        // additional cleanup needed to remove location tooltips
        // that are still open
        var $location = $($entry.find('.location')[0]);
        if ($location) {
            if ($location.data('tooltip')) {
                $location.data('tooltip').hide();
            }
        }
    }
};


/**
Toggles Entry Favorite CSS on/off

@param {Object} - the jQuery event Object
*/
ACT.toggle_entry_favorite = function (evt) {
    evt.preventDefault();
    var $btn = $(this);
    var $entry = $btn.closest('.entry');
    var $image = $($btn.find('i')[0]);
    if ($entry.hasClass('entryUnread')) {
        $entry.removeClass('entryUnread');
    }
    if ($entry.hasClass('entryFavorite')) {
        $entry.removeClass('entryFavorite');
        $image.removeClass('icon-thumbs-down').addClass('icon-thumbs-up');
    } else {
        $entry.addClass('entryFavorite');
        $image.removeClass('icon-thumbs-up').addClass('icon-thumbs-down');
    }
};


/**
Toggles the Entry Content show/hide as well as marking the Entry read

@param {Object} - the jQuery event Object
*/
ACT.toggle_entry_content = function (evt) {
    evt.preventDefault();
    var $btn = $(this);
    var $entry = $btn.closest('.entry');
    if ($entry.hasClass('entryUnread')) {
        $entry.removeClass('entryUnread');
    }
    var $content = $($entry.find('.content')[0]);
    var $updated_span = $($entry.find('.updated > span')[0]);
    if ($btn.hasClass('well-title-highlight')) {
        $btn.removeClass('well-title-highlight');
        $content.css('display', 'none');
        // changing back to datetime since
        if ($updated_span) {
            var dt_since = $updated_span.attr('data-since');
            if (dt_since) {
                $updated_span.text($updated_span.attr('data-since'));
            }
        }
    } else {
        $btn.addClass('well-title-highlight');
        $content.css('display', '');
        // changing to full datetime display for clarity and reporting
        if ($updated_span) {
            $updated_span.attr('data-since', $updated_span.text());
            $updated_span.text($updated_span.attr('title'));
        }
    }
};


/**
Create the location tooltip on demand to display the location map on
demand, saves resources compared to always creating.

@param {Object} - the jQuery event Object
*/
ACT.create_location_tooltip = function (evt) {
    evt.preventDefault();
    if (!$(this).data('tooltip')) {
        var location = $(this).attr('data-location');
        if (!location) {
            console.error('Location for tooltip is missing');
            return;
        }
        var img_html = '<button class="btn btn-mini zoomIn"><i class="icon-plus"></i></button>' +
            '<button class="btn btn-mini zoomOut"><i class="icon-minus"></i></button>' +
            '<img alt="Location" data-zoom="7" src="' + ACT.STATIC_MAP_URL +
            '?sensor=false&size=250x250&zoom=7&markers=' + location +
            '" height="250" width="250">';
        $(this).tooltip({'html': true, 'title': img_html, 'placement': 'left',
            'trigger': 'click'});
        // show tip this time because the click has already been sent
        // whereas future clicks will toggle show/hide properly
        $(this).data('tooltip').show();
    }
};


/**
Modifies the zoom value used for the static map image to
allow zoom in/out of the location map.

@param {Object} - the jQuery event Object
*/
ACT.update_location_zoom = function (evt) {
    evt.preventDefault();
    var $img = $(this).parent().find('img');
    var img_src = $img.attr('src');
    var img_zoom = parseInt($img.attr('data-zoom'), 10);
    // ignore if too far out or too close
    if (evt.data.action === 'in') {
        if (img_zoom > 19) {
            return;
        }
        var new_zoom = img_zoom + 1;
    } else {
        if (img_zoom < 4) {
            return;
        }
        var new_zoom = img_zoom - 1;
    }
    var new_src = img_src.replace('zoom=' + img_zoom, 'zoom=' + new_zoom);
    $img.attr('src', new_src);
    $img.attr('data-zoom', new_zoom);
};


/**
Expands the details and content for all Entries

@param {Object} - the jQuery event Object
*/
ACT.expand_all_entries = function (evt) {
    evt.preventDefault();
    $('.entry').each(function () {
        var $entry = $(this);
        var $btn = $($entry.find('.minMaxBtn')[0]);
        var $image = $($btn.find('i')[0]);
        var $details = $($entry.find('.details')[0]);
        if ($details.css('display') === 'none') {
            $details.css('display', '');
            $image.removeClass('icon-plus-sign').addClass('icon-minus-sign');
        }
        var $content = $($entry.find('.content')[0]);
        if ($content.css('display') === 'none') {
            $content.css('display', '');
        }
        // changing to full datetime display for clarity and reporting
        var $updated_span = $($entry.find('.updated > span')[0]);
        if ($updated_span) {
            $updated_span.text($updated_span.attr('title'));
        }
    });
};


/**
Collapses the details and content for all Entries

@param {Object} - the jQuery event Object
*/
ACT.collapse_all_entries = function (evt) {
    evt.preventDefault();
    $('.entry').each(function () {
        var $entry = $(this);
        var $btn = $($entry.find('.minMaxBtn')[0]);
        var $image = $($btn.find('i')[0]);
        var $details = $($entry.find('.details')[0]);
        if ($details.css('display') !== 'none') {
            $details.css('display', 'none');
            $image.removeClass('icon-minus-sign').addClass('icon-plus-sign');
        }
        var $content = $($entry.find('.content')[0]);
        if ($content.css('display') !== 'none') {
            $content.css('display', 'none');
        }
    });
    // reset datetime display
    ACT.feed.update_all_since();
};


/**
Marks all Entries as read

@param {Object} - the jQuery event Object
*/
ACT.mark_all_read = function (evt) {
    evt.preventDefault();
    $('.entry').each(function () {
        var $entry = $(this);
        if ($entry.hasClass('entryUnread')) {
            $entry.removeClass('entryUnread');
        }
    });
};


/**
Clears all Entries and resets feed fetch attributes

@param {Object} - the jQuery event Object
*/
ACT.clear_all_reset = function (evt) {
    evt.preventDefault();
    var proceed = confirm('Clear All Entries and Reset Feeds?');
    if (proceed) {
        console.debug('Clear all and reset');
        ACT.feed.clear_all();
        // make sure the feed clearing doesn't leave anything behind
        var leftovers = $('.entry');
        if (leftovers.length > 0) {
            console.error('Leftovers after clearing feed');
            for (var i = 0; i < leftovers.length; i++) {
                $(leftovers[i]).empty().remove();
            }
        }
        $('.entrySeparator').remove();
    }
};


/**
Searches through the Entry listing to find a match and then expand it
and bring it into view.  Used for EntryID and Author searches so far.

@param {Object} - the jQuery event Object
*/
ACT.search_related_entries = function (evt) {
    evt.preventDefault();
    var $entry = $(this).closest('.entry');
    var current_target = $entry.find('.' + evt.data.target).text();
    
    function search(idx, elem) {
        var new_target = $(elem).find('.' + evt.data.target).text();
        if (new_target === current_target) {
            ACT.goto_entry($(elem));
            // stops the each loop upon reaching the first match
            return false;
        }
        // not found, continue searching
        return true;
    }
    
    // new moves upward in the DOM order from the location of this
    // selected entry, while old moves down
    if (evt.data.order === 'new') {
        $entry.prevAll('.entry').each(search);
    } else {
        $entry.nextAll('.entry').each(search);
    }
};


/**
After an Author is selected from the Author List, searches through the Entry
listing to find the first author match and bring it into view.

@param {Object} - the jQuery event Object
*/
ACT.search_first_author = function (evt) {
    evt.preventDefault();
    // need to remove the <span> with the count
    var $author_copy = $(this).clone();
    $author_copy.children().remove();
    var author_name = $author_copy.text();
    // start out by testing the first entry in the listing
    var $first_entry = $('.entry').first();
    var first_author = $first_entry.find('.author').text();
    if (first_author === author_name) {
        ACT.goto_entry($first_entry);
        return;
    }
    
    function search(idx, elem) {
        var new_target = $(elem).find('.author').text();
        if (new_target === author_name) {
            ACT.goto_entry($(elem));
            // stops the each loop upon reaching the first match
            return false;
        }
        // not found, continue searching
        return true;
    }
    
    $first_entry.nextAll('.entry').each(search);
};


/**
Adds new author uri's to the filter list

@param {Object} - the jQuery event Object
*/
ACT.filter_author = function (evt) {
    evt.preventDefault();
    var author_uri = $(this).attr('data-uri');
    var author_name = $(this).closest('.entry').find('.author').text();
    if (author_uri) {
        if (ACT.FILTER_AUTHORS.indexOf(author_uri) === -1) {
            ACT.FILTER_AUTHORS.push(author_uri);
            // display the name in the filter info current list
            if (author_name) {
                // shorten the name for display, add .. later?
                author_name = author_name.substring(0, 16);
                var filter_html = '<li class="filterInfoItem" data-type="author" ' +
                    'data-uri="' + author_uri + '">' +
                    '<i class="icon-remove filterRemove"></i> ' +
                    author_name + '</li>';
                $('#filterInfoTitle').after(filter_html);
            }
        }
        ACT.feed.remove_entries_by_author(author_uri);
        console.debug('Filter Author: ' + author_uri);
        alert('This Author will now be filtered in further updates');
    }
};


/**
Adds new feed uri's to the filter list

@param {Object} - the jQuery event Object
*/
ACT.filter_feed = function (evt) {
    evt.preventDefault();
    var feed_uri = $(this).attr('data-uri');
    var feed_name = $(this).attr('data-name');
    if (feed_uri) {
        if (ACT.FILTER_FEEDS.indexOf(feed_uri) === -1) {
            ACT.FILTER_FEEDS.push(feed_uri);
            // display the name in the filter info current list
            if (feed_name) {
                // shorten the name for display, add .. later?
                feed_name = feed_name.substring(0, 11);
                var filter_html = '<li class="filterInfoItem" data-type="feed" ' +
                    'data-uri="' + feed_uri + '">' +
                    '<i class="icon-remove filterRemove"></i> Feed = ' +
                    feed_name + '</li>';
                $('#filterInfoTitle').after(filter_html);
            }
        }
        console.debug('Filter Feed: ' + feed_uri);
        alert('This Feed will now be filtered in further updates');
    }
};


/**
Remove a single filter, either author or feed

@param {Object} - the jQuery event Object
*/
ACT.remove_filter = function (evt) {
    evt.preventDefault();
    var $filter = $(this).parent();
    var f_type = $filter.attr('data-type');
    var f_uri = $filter.attr('data-uri');
    if (f_type === 'author') {
        var f_loc = ACT.FILTER_AUTHORS.indexOf(f_uri);
        if (f_loc !== -1) {
            ACT.array_remove(ACT.FILTER_AUTHORS, f_loc);
            $filter.remove();
            console.debug('Author ' + f_uri + ' filter removed');
            alert('Author filter removed for further updates');
        }
    } else if (f_type === 'feed') {
        var f_loc = ACT.FILTER_FEEDS.indexOf(f_uri);
        if (f_loc !== -1) {
            ACT.array_remove(ACT.FILTER_FEEDS, f_loc);
            $filter.remove();
            console.debug('Feed ' + f_uri + ' filter removed');
            alert('Feed filter removed for further updates');
        }
    }
};


/**
Remove all filters, author and feed

@param {Object} - the jQuery event Object
*/
ACT.remove_all_filters = function (evt) {
    evt.preventDefault();
    var proceed = confirm('Remove All Filters?');
    if (proceed) {
        ACT.FILTER_AUTHORS = [];
        ACT.FILTER_FEEDS = [];
        $('.filterInfoItem').remove();
        console.debug('Removed all filters');
        alert('All filters have been removed for futher updates');
    }
};


/**
Searches for Entries with matching schemes and terms, such as
categories and authors and expands/collapses accordingly.

@param {Object} - the jQuery event Object
*/
ACT.search_related_terms = function (evt) {
    evt.preventDefault();
    var scheme = $(this).attr('data-scheme');
    var term = $(this).attr('data-term');
    var matches = ACT.feed.term_search(scheme, term);
    for (var i = 0; i < matches.length; i++) {
        if (evt.data.action === 'collapse') {
            matches[i].hide();
        } else {
            matches[i].show();
        }
    }
};


/**
(Re)Loads the activity feed(s), either manually or via timer

@param {Object} - the jQuery event Object, optional
*/
ACT.load_activity_feed = function (evt) {
    // handle timer calls without an event object
    if (evt && evt.preventDefault) {
        evt.preventDefault();
    }
    // timer setInterval var ACT.load_activity_feed_timer
    if (ACT.feed) {
        ACT.feed.load_feed();
    }
    // start the timer after the first manual run to allow users to adjust
    // settings and other setup prior to loading
    ACT.load_activity_feed_start();
};


/**
Toggles the loading icon beside the Activity title on/off
*/
ACT.toggle_loading_icon = function () {
    var $icon = $("#loadIcon");
    if ($icon.hasClass('icon-refresh')) {
        $icon.removeClass('icon-refresh').addClass('reloading-icon');
    } else {
        $icon.removeClass('reloading-icon').addClass('icon-refresh');
    }
};


/* Timers */

/**
Start the auto poll timer
*/
ACT.load_activity_feed_start = function () {
    if (!ACT.load_activity_feed_timer) {
        if (ACT.ENTRY_AUTO_POLL) {
            ACT.load_activity_feed_timer = setInterval(ACT.load_activity_feed,
                ACT.ENTRY_POLL * 60000);
            console.debug('Started auto poll every ' + ACT.ENTRY_POLL +
                ' minutes');
        }
    }
};


/**
Stop the auto poll timer
*/
ACT.load_activity_feed_stop = function () {
    if (ACT.load_activity_feed_timer) {
        clearInterval(ACT.load_activity_feed_timer);
        ACT.load_activity_feed_timer = null;
    }
};


/**
Periodically updates the since values in order to keep them up to date
*/
ACT.update_all_since = function () {
    // timer run, setInterval var ACT.update_all_since_timer
    if (ACT.feed) {
        ACT.feed.update_all_since();
    }
};



/**
Initialize the application
*/
$(document).ready(function () {
    
    $('#expandAll').click(ACT.expand_all_entries);
    $('#collapseAll').click(ACT.collapse_all_entries);
    $('#readAll').click(ACT.mark_all_read);
    $('#clearAll').click(ACT.clear_all_reset);
    $('#removeAllFilters').click(ACT.remove_all_filters);
    $('#saveSettings').click(ACT.save_new_settings);
    $('#settingsModal').on('shown', ACT.load_current_settings);
    $('#activityMapModal').on('shown', ACT.load_activity_map);
    // using $(document) to ensure that when new entries are added to the DOM,
    // the click handlers will be automatically added
    $(document).on('click', '.minMaxBtn', ACT.toggle_entry_details);
    $(document).on('click', '.favorite', ACT.toggle_entry_favorite);
    $(document).on('click', '.entryIdNew', {'target': 'entryID', 'order': 'new'},
        ACT.search_related_entries);
    $(document).on('click', '.entryIdOld', {'target': 'entryID', 'order': 'old'},
        ACT.search_related_entries);
    $(document).on('click', '.authorNew', {'target': 'author', 'order': 'new'},
        ACT.search_related_entries);
    $(document).on('click', '.authorOld', {'target': 'author', 'order': 'old'},
        ACT.search_related_entries);
    $(document).on('click', '.title', ACT.toggle_entry_content);
    $(document).on('click', '.filterAuthorLoad', ACT.filter_author);
    $(document).on('click', '.filterFeedLoad', ACT.filter_feed);
    $(document).on('click', '.matchExpand', {'action': 'expand'},
        ACT.search_related_terms);
    $(document).on('click', '.matchCollapse', {'action': 'collapse'},
        ACT.search_related_terms);
    $(document).on('click', '.location', ACT.create_location_tooltip);
    $(document).on('click', '.zoomIn', {'action': 'in'},
        ACT.update_location_zoom);
    $(document).on('click', '.zoomOut', {'action': 'out'},
        ACT.update_location_zoom);
    // for when new filtering dropdown menu listings are dynamically added
    $(document).on('click', '.filterRemove', ACT.remove_filter);
    // Entries menu, Author List sub-menu
    $(document).on('click', '.authorListing', ACT.search_first_author);
    
    // compile the new entry handlebar template so its ready for use
    ACT.entry_template = Handlebars.compile($('#entry-template').html());
    // create the new feed instance
    ACT.feed = new ACT.FeedClass();
    // manual load feed handler
    $('#loadActivity').click(ACT.load_activity_feed);
    
    // every 5 mins, update the time based since field
    ACT.update_all_since_timer = setInterval(ACT.update_all_since, 300000);
    
});
