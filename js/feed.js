/**
MASAS Activity Log - Feed
Updated: Oct 21, 2013
Independent Joint Copyright (c) 2012-2013 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

@requires entry.js
*/

/*global ACT,jQuery,$ */

/**
Global Namespace
*/
//var ACT = window.ACT || {};


/**
Feed Class
*/
ACT.FeedClass = function () {
    // static attributes
    this.name = 'FeedClass';
    // simple constructor used
    if (!(this instanceof ACT.FeedClass)) {
        throw new Error('constructor error');
    }
    // run init if available
    if (typeof this.init === 'function') {
        this.init.apply(this, arguments);
    }
};


/**
Class initialization

@param {Object} - instance setup options
*/
ACT.FeedClass.prototype.init = function (options) {
    // currently only supporting english
    this.lang = (options && options.lang) ? options.lang : 'en';
    // used to prevent conflicting modifications of feed data
    this.locked = false;
    // instance attributes used to hold overall state
    this.seq_num = 1;
    this.last_updated = null;
    this.entries = [];
    this.unique_ids = [];
    // instance attributes that are temporary storage during feed loading
    this.fetch_num = 1;
    this.fetch_entries = [];
};


/**
Loads a feed(s) by creating the request strings and sending via AJAX
async requests

@param {Object} - options to use for this specific feed load
*/
ACT.FeedClass.prototype.load_feed = function (options) {
    var lang = this.lang;
    var urls = (options && options.url) ? options.url : ACT.FEED_URLS;
    var secret = (options && options.secret) ? options.secret : ACT.USER_SECRET;
    var bbox = (options && options.bbox) ? options.bbox : ACT.MAP_DEFAULT_VIEW;
    var limit = (options && options.limit) ? options.limit : ACT.ENTRY_MAX;
    var history = (options && options.history) ? options.history : ACT.ENTRY_HISTORY;
    if (!urls) {
        throw new Error('load_feed requires a url');
    }
    if (!secret) {
        throw new Error('load_feed requires a secret');
    }
    if (!lang) {
        throw new Error('load_feed requires a lang');
    }
    if (!limit) {
        limit = 100;
    } else {
        if (limit > 1000) {
            throw new Error('limit setting exceeds max allowable');
        }
    }
    var since = null;
    var range_start = null;
    var range_end = null;
    if (ACT.ENTRY_AGE === 999) {
        if (ACT.ENTRY_DATE_START) {
            range_start = ACT.iso_utc_format(ACT.ENTRY_DATE_START);
        }
        if (ACT.ENTRY_DATE_END) {
            range_end = ACT.iso_utc_format(ACT.ENTRY_DATE_END);
        }
    } else {
        if (this.last_updated) {
            since = this.last_updated;
        } else {
            if (ACT.ENTRY_AGE !== 0) {
                since = this.generate_since_time(new Date(), ACT.ENTRY_AGE * 60);
            }
        }
    }
    // locking used to ensure no modification conflicts
    if (this.locked) {
        // send errors for now to see how often this occurs
        console.error('Load Feed: Locked');
        return;
    }
    this.locked = true;
    // setup for this load run, cleans up any leftovers from failed attempts
    //TODO: since the last runs values aren't cleared until here, could
    //      potentially review and note them when reporting any errors?
    //      Otherwise, just clear earlier instead to save mem.
    this.fetch_num = 1;
    this.fetch_entries = [];
    
    ACT.toggle_loading_icon();
    var all_feeds_filtered = true;
    
    // supports multiple hubs
    for (var i = 0; i < urls.length; i++) {
        // feed filtering support
        if (ACT.FILTER_FEEDS) {
            if (ACT.FILTER_FEEDS.indexOf(urls[i]) !== -1) {
                continue;
            }
        }
        all_feeds_filtered = false;
        // assemble the query string arguments
        var feed_url = urls[i] + '?lang=' + lang + '&limit=' + limit;
        if (bbox) {
            feed_url += '&bbox=' + bbox;
        }
        //NOTE: history can result in loading very old entries
        if (history) {
            feed_url += '&history=all';
        }
        // if 0 has been used for ENTRY_AGE, that means ask for current and
        // so don't add dtsince.  Should resume adding dtsince after the
        // initial load
        if (since) {
            feed_url += '&dtsince=' + since;
        }
        // otherwise if 999 was used for ENTRY_AGE, use a date range instead
        if (range_start && range_end) {
            feed_url += '&dtstart=' + range_start + '&dtend=' + range_end;
        }
        // add any author uri's to ignore
        if (ACT.FILTER_AUTHORS && ACT.FILTER_AUTHORS.length > 0) {
            feed_url += '&not_author=';
            if (ACT.FILTER_AUTHORS.length === 1) {
                feed_url += ACT.FILTER_AUTHORS[0];
            } else {
                feed_url += ACT.FILTER_AUTHORS.join(',');
            }
        }
        console.debug('Loading Feed: ' + feed_url);
        //NOTE: dashboard doesn't use header but query secret for authentication
        feed_url += '&secret=' + secret;
        // support proxies
        if (ACT.AJAX_PROXY_URL) {
            feed_url = ACT.AJAX_PROXY_URL + encodeURIComponent(feed_url);
        }
        $.ajax({
            type: 'GET',
            'url': feed_url,
            //headers: {'Authorization': 'MASAS-Secret ' + secret},
            dataType: 'text',
            // making sure 'this' in parse_feed refers to FeedClass instance
            context: this,
            // 1 min
            timeout: 60000,
            error: this.feed_error,
            success: this.update_feed
        });
    }
    
    // handle cases where all feeds end up being filtered
    if (all_feeds_filtered) {
        this.locked = false;
        ACT.toggle_loading_icon();
        alert('Unable to load new Entries, all Feeds have been filtered');
    }
};


/**
AJAX Error Handler

@param {Object} - the XHR object with error details
*/
ACT.FeedClass.prototype.feed_error = function (xhr) {
    console.error('Load Feed Error: ' + xhr.status + ' ' + xhr.statusText);
    this.locked = false;
    ACT.toggle_loading_icon();
    alert('Error Loading Feeds.  Check Settings and Try Again.');
    return;
};


/**
AJAX Success Handler.  Parses returned results, queuing multiple requests
until they have all returned, then adds the new Entries to the page.

@param {String} - the text results from the request 
@param {Object} - the status object with status info
@param {Object} - the XHR object with request details
*/
ACT.FeedClass.prototype.update_feed = function (data, status, xhr) {
    var new_entries = this.parse_feed(data);
    // extend the existing array
    this.fetch_entries.push.apply(this.fetch_entries, new_entries);
    // when there are multiple hubs, we wait until all async fetch requests
    // have completed before parsing the final results 
    var fetch_total = ACT.FEED_URLS.length;
    if (ACT.FILTER_FEEDS && ACT.FILTER_FEEDS.length > 0) {
        // account for filtered hubs in the fetch count
        fetch_total -= ACT.FILTER_FEEDS.length;
    }
    if (this.fetch_num < fetch_total) {
        this.fetch_num += 1;
        return;
    }
    
    var all_entries = this.review_entries(this.fetch_entries);
    if (all_entries.length > 0) {
        console.debug('Adding ' + all_entries.length + ' new entries');
        // will add this array of html to the DOM all at once as its faster
        // than individually, chosen for now over using a document fragment
        // ie. document.createDocumentFragment()
        var new_html = [];
        var new_unique_id = null;
        for (var i = 0; i < all_entries.length; i++) {
            new_unique_id = all_entries[i].entry_id + ',' +
                all_entries[i].entry_updated;
            this.unique_ids.push(new_unique_id);
            all_entries[i].seq = this.seq_num;
            new_html.push(all_entries[i].generate_html());
            this.seq_num += 1;
        }
        // add a bit of separation between this grouping of entries and
        // the previously retrieved group
        new_html.push('<div class="entrySeparator"></div>');
        // add to the top of the page
        var container = $('#entries');
        container.prepend(new_html.join(''));
        // extend the existing array
        this.entries.push.apply(this.entries, all_entries);
    }
    // clean out any old entries exceeding the max
    this.clean_old_entries();
    // update the author list with current information
    this.update_author_list();
    // set the value that will be used for the next feed load, using server's
    // time from the last successful request
    var last_date = null;
    try {
        last_date = new Date(xhr.getResponseHeader('Date'));
    } catch (err) {
        console.error('Date Error: ' + err);
    }
    if (!last_date) {
        // fall back to using local client's time
        last_date = new Date();
    }
    // saving the last_updated value as 5 minutes in the past to help
    // account for processing delays and server time differences.  May
    // result in some duplicates during the next request for Entries
    // updated in the interm but they should be filtered out.
    this.last_updated = this.generate_since_time(last_date, 5);
    // clear the feed loading lock once all async requests are done
    this.locked = false;
    ACT.toggle_loading_icon();
};


/**
Parses the returned XML Atom feed.  Generates Entry instances and they
in turn parse the XML for each individual Entry.

@param {String} - the text results from the request
@return {Array} - the array of new Entry Objects
*/
ACT.FeedClass.prototype.parse_feed = function (data) {
    // if 'xml' were specified for the .ajax dataType, then jQuery would
    // try to return data as parsed XML.  However not using that to ensure
    // browsers like IE are accomodated with our own parser
    var xml_doc = ACT.parse_xml(data);
    if (!xml_doc) {
        console.error('Feed Parsing Error');
        return;
    }
    var root = jQuery('feed', xml_doc).eq(0);
    var feed_link = jQuery(root).find('link:first').eq(0).attr('href');
    if (feed_link && ACT.FEED_NAMES) {
        var feed_name = ACT.FEED_NAMES[feed_link];
    }
    
    var new_entries = [];
    jQuery('entry', xml_doc).each(function () {
        try {
            var new_entry = new ACT.EntryClass(this);
            // when multiple feeds, show the name and an option to filter
            if (feed_link && feed_name && ACT.FEED_URLS.length > 1) {
                new_entry.feed_link = feed_link;
                new_entry.feed_name = feed_name;
            }
            new_entries.push(new_entry);
        } catch (err) {
            console.error('Parse Entry Error: ' + err);
        }
    });
    
    return new_entries;
};


/**
Reviews the new Entries by filtering and sorting them.

@param {Array} - the array of new Entry Objects
@return {Array} - the array of newly reviewed Entry Objects
*/
ACT.FeedClass.prototype.review_entries = function (new_entries) {
    // because we can ask the Hub for ?history=all in order to capture changes
    // that occur between polling attempts, and if loading from multiple hubs
    // its expected there will be a lot of duplicates, so filter these out first
    var filtered_entries = [];
    var new_ids = [];
    for (var i = 0; i < new_entries.length; i++) {
        var new_unique_id = new_entries[i].entry_id + ',' +
            new_entries[i].entry_updated;
        // comparing to existing IDs from previous runs and also to
        // IDs that may be in this new batch.
        //NOTE: if an ID drops off because it ages out of the max saved
        //      list, it may appear again with ?history=all
        if (this.unique_ids.indexOf(new_unique_id) === -1 &&
        new_ids.indexOf(new_unique_id) === -1) {
            filtered_entries.push(new_entries[i]);
            new_ids.push(new_unique_id);
        }
    }
    // again because of the use of ?history=all, there may be changes
    // grouped together instead of in a strictly chronological order,
    // so perform a custom sort, the timestamps should be reasonably
    // unique and definitive.  The ordering should be newest first
    // [9 AM, 8 AM, ...] based on - prefix for array_sort
    
    return filtered_entries.sort(ACT.array_sort('-timestamp'));
};


/**
After new Entries are added, check to see if the max number of Entries
to save/display is exceeded and if so, remove the oldest Entries.
NOTE: load_feed should have set a lock prior to this occuring so that
other modifications won't take place
*/
ACT.FeedClass.prototype.clean_old_entries = function () {
    var current_length = this.entries.length;
    if (current_length <= ACT.ENTRY_MAX) {
        return;
    }
    var remove_num = current_length - ACT.ENTRY_MAX;
    // no cleanup needed for unique_ids.  The number and ordering for both
    // unique_ids and entries should be the same.
    this.unique_ids.splice(0, remove_num);
    // entry cleanup
    var old_entries = this.entries.splice(0, remove_num);
    console.debug('Removing ' + old_entries.length + ' old entries');
    for (var i = 0; i < old_entries.length; i++) {
        try {
            old_entries[i].remove();
        } catch (err) {
            console.error('Clean Old Error: ' + err);
        }
    }
    // remove any separators AFTER last Entry, otherwise they can build
    // up at the bottom
    $('#entry' + this.entries[0].seq).nextAll('.entrySeparator').remove();
};


/**
Update the Author List with all current unique authors based on the
existing Entries.
*/
ACT.FeedClass.prototype.update_author_list = function () {
    $('#author-list').children().remove();
    var current_authors = {};
    for (var i = 0; i < this.entries.length; i++) {
        if (current_authors[this.entries[i].entry_author_name]) {
            current_authors[this.entries[i].entry_author_name] += 1;
        } else {
            current_authors[this.entries[i].entry_author_name] = 1;
        }
    }
    jQuery.each(current_authors, function (key, value) {
        $('#author-list').append('<li><a href="#" class="authorListing">' +
            key + '<span class="authorListCount">' + value + '</span></a></li>');
    });
};


/**
Generates a datetime string to use as the feed parameter dtsince= based
on a given time object and minutes in the past.  Used both upon initial
load to gather past history, and for subsequent reloads to only gather
changes since the last reload attempt.

@param {Object} - datetime object with starting point for time calculations
@param {Integer} - how many minutes in the past to use for calculating
the since value
@return {String} - an ISO formatted string in UTC
*/
ACT.FeedClass.prototype.generate_since_time = function (now_time, mins) {
    var now_epoch = now_time.getTime();
    // number of epoch milliseconds based on issued minutes difference
    var diff_epoch = now_epoch - (60000 * mins);
    var since_date = new Date();
    since_date.setTime(diff_epoch);
    
    return ACT.iso_utc_format(since_date);
};


/**
Periodically updated the since display values for all Entries.
*/
ACT.FeedClass.prototype.update_all_since = function () {
    var current = parseInt(new Date().getTime(), 10);
    for (var i = 0; i < this.entries.length; i++) {
        try {
            this.entries[i].update_since(current);
        } catch (err) {
            console.error('Update Since Error: ' + err);
        }
    }
};


/**
Remove all Entries and reset instance attributes, used to start
somewhat fresh without reloading page and losing settings.
*/
ACT.FeedClass.prototype.clear_all = function () {
    // locking used to ensure no modification conflicts
    //TODO: if locking gets messed up somehow, should this function
    //      be allowed to proceed regardless of the lock so that it
    //      can clean up a lock properly?
    if (this.locked) {
        // send errors for now to see how often this occurs
        console.error('Clear All: Locked');
        alert('Unable to clear entries while a background operation is ' +
            'running, please wait and try again.');
        return;
    }
    this.locked = true;
    for (var i = 0; i < this.entries.length; i++) {
        try {
            this.entries[i].remove();
        } catch (err) {
            console.error('Clean All Error: ' + err);
        }
    }
    this.seq_num = 1;
    this.last_updated = null;
    this.entries = [];
    this.unique_ids = [];
    this.fetch_num = 1;
    this.fetch_entries = [];
    this.locked = false;
};


/**
Searches all Entries and returns those that have a matching
term based on a given scheme, such as categories or authors.

@param {String} - the scheme value to use for the search
@param {String} - the term value to search for
@return {Array} - the array of Entries that do match
*/
ACT.FeedClass.prototype.term_search = function (scheme, term) {
    var matches = [];
    for (var i = 0; i < this.entries.length; i++) {
        if (this.entries[i][scheme].indexOf(term) !== -1) {
            matches.push(this.entries[i]);
        }
    }
    
    return matches;
};


/**
Remove existing Entries based on Author URI

@param {String} - the author URI
*/
ACT.FeedClass.prototype.remove_entries_by_author = function (author) {
    // locking used to ensure no modification conflicts
    if (this.locked) {
        // send errors for now to see how often this occurs
        console.error('Remove Authors: Locked');
        alert('Unable to finish filtering while a background operation is ' +
            'running, please wait and try again.');
        return;
    }
    this.locked = true;
    var remove_count = 0;
    // iterating in reverse so that removing an array item doesn't
    // mess up the overall index ordering
    for (var i = this.entries.length - 1; i >= 0; i--) {
        if (this.entries[i].entry_author_uri === author) {
            try {
                // clean out unique ids first
                var unique_idx = this.unique_ids.indexOf(this.entries[i].entry_id +
                    ',' + this.entries[i].entry_updated);
                if (unique_idx !== -1) {
                    this.unique_ids.splice(unique_idx, 1);
                }
                // removing entry from DOM
                this.entries[i].remove();
                // finally removing entry
                this.entries.splice(i, 1);
                remove_count++;
            } catch (err) {
                console.error('Author Remove Error: ' + err);
            }
        }
    }
    console.debug('Removed ' + remove_count + ' Author Entries');
    this.locked = false;
};


/**
Export a GeoJSON string of point features from current Entry locations.
*/
ACT.FeedClass.prototype.export_entry_locations = function () {
    var geojson_string = '{"type": "FeatureCollection", "features": [\n';
    for (var i = 0; i < this.entries.length; i++) {
        var loc_point = this.entries[i].entry_location.split(/,/);
        geojson_string += '{"type": "Feature", "geometry": {"type": "Point", "coordinates": [';
        geojson_string += loc_point[1] + ', ' + loc_point[0] + ']}}';
        if (i < (this.entries.length - 1)) {
            geojson_string += ',\n';
        }
    }
    geojson_string += '\n]}';
    
    return geojson_string;
};
