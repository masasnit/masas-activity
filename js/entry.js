/**
MASAS Activity Log - Entry
Updated: Oct 21, 2013
Independent Joint Copyright (c) 2012-2013 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
*/

/*global ACT,jQuery,$,OpenLayers */

/**
Global Namespace
*/
//var ACT = window.ACT || {};


/**
Entry Class
*/
ACT.EntryClass = function () {
    // static attributes
    this.name = 'EntryClass';
    // simple constructor used
    if (!(this instanceof ACT.EntryClass)) {
        throw new Error('constructor error');
    }
    // run init if available
    if (typeof this.init === 'function') {
        this.init.apply(this, arguments);
    }
};


/**
Class initialization

@param {XMLDoc} - parses a provided XML Doc
*/
ACT.EntryClass.prototype.init = function (entry_xml) {
    if (entry_xml) {
        this.parse_xml(entry_xml);
    }
};


/**
Parses a provided XML Doc and creates instance attributes accordingly.  Most
are used for the html generation template.

@param {XMLDoc} - parses a provided XML Doc
*/
ACT.EntryClass.prototype.parse_xml = function (entry_xml) {
    var $entry = jQuery(entry_xml);   
    this.entry_id = $entry.find('id').eq(0).text();
    this.entry_title = $entry.find('title').eq(0).text();
    this.entry_content = $entry.find('content').eq(0).text();
    var $author_elem = $entry.find('author').eq(0);
    this.entry_author_name = $author_elem.find('name').eq(0).text();
    this.entry_author_uri = $author_elem.find('uri').eq(0).text();
    this.entry_categories = [];
    var cat_category = $entry.find('[scheme="masas:category:category"]');
    if (cat_category.length !== 0) {
        //TODO: support multiple category values for this scheme
        this.entry_categories.push(cat_category.eq(0).attr('term'));
    }
    var cat_severity = $entry.find('[scheme="masas:category:severity"]');
    if (cat_severity.length !== 0) {
        this.entry_categories.push(cat_severity.eq(0).attr('term'));
    }
    var cat_colour = $entry.find('[scheme="masas:category:colour"]');
    if (cat_colour.length !== 0) {
        this.entry_categories.push(cat_colour.eq(0).attr('term'));
    }
    var cat_status = $entry.find('[scheme="masas:category:status"]');
    if (cat_status.length !== 0) {
        var cat_status_term = cat_status.eq(0).attr('term');
        if (cat_status_term !== 'Actual') {
            this.entry_categories.push(cat_status_term);
        }
    }
    // supports simple display of attachments for now with a short name
    // provided if the content type is found
    this.entry_attachments = [];
    var enclosures = $entry.find('link[rel="enclosure"]');
    if (enclosures.length !== 0) {
        for (var i = 0; i < enclosures.length; i++) {
            var attach_info = {'link': $(enclosures[i]).attr('href') +
                '?secret=' + ACT.USER_SECRET };
            var attach_type = $(enclosures[i]).attr('type');
            if (ACT.ATTACH_TYPES[attach_type]) {
                attach_info['type'] = ACT.ATTACH_TYPES[attach_type];
            } else {
                attach_info['type'] = 'Attach';
            }
            this.entry_attachments.push(attach_info);
        }
    }
    // find the simple location point used to represent this Entry with a
    // Google static map image
    this.entry_location = this.parse_location($entry);
    // only the updated value is used, different formats needed for
    // various purposes
    var updated_val = $entry.find('updated').eq(0).text();
    this.entry_updated = updated_val;
    this.timestamp = ACT.iso_utc_parse(updated_val);
    this.entry_updated_display = jQuery.format.date(this.timestamp, 'MMM d h:mm a');
    this.since = this.generate_since();
    var overview_text = '';
    // helps distinguish between a new entry and an update
    var has_history = $entry.find('link[rel="history"]');
    if (has_history && has_history.length > 0) {
        // may not be needed, for example with CAP auto-generated entries
        if (this.entry_title.search(/update/i) === -1 && this.entry_title.search(/cancel/i) === -1) {
            overview_text += 'Update: ';
        }
    }
    // since using CSS overflow:hidden doesn't work with the fluid grid
    // in Bootstrap, using some defaults here for the overview cutoff
    if (this.entry_title.length > 40) {
        overview_text += this.entry_title.substring(0, 40) + '...';
    } else {
        overview_text += this.entry_title;
    }
    this.entry_overview = overview_text;
    // the full link to the Entry with secret
    var $entry_edit_link = $($entry.find('link[rel="edit"]')[0]);
    if ($entry_edit_link) {
        this.entry_link = $entry_edit_link.attr('href') +
            '?secret=' + ACT.USER_SECRET;
    }
};


/**
Determines a point location to use on a map to represent this Entry.
Parses the GeoRSS geometry and returns the centroid value.

@param {Object} - the Entry jQuery object
@return {String} - a point location value
*/
ACT.EntryClass.prototype.parse_location = function ($entry) {
    var geom = null;
    var location = null;
    
    // used to search for namespaced elements such as geo:point because
    // some browsers like Chrome don't accept the jQuery selector
    // .find('geo\\:point')
    function geo_search(target) {
        var result = null;
        $entry.find('*').each(function () {
            var tag = this.tagName.toLowerCase();
            //TODO: support <geometry> and <clarknamespace:geometry> as well
            //      with an .endswith(geometry) test instead
            if (tag.indexOf(target) !== -1) {
                result = $(this).text();
            }
        });
        return result;
    }
    
    // point and circle values are easiest and don't need much processing
    var point = geo_search('georss:point');
    if (point && point.length > 0) {
        location = OpenLayers.String.trim(point).replace(' ', ',');
        return location;
    }
    
    var circle = geo_search('georss:circle');
    if (circle && circle.length > 0) {
        var values = OpenLayers.String.trim(circle).split(/\s+/);
        location = values[0] + ',' + values[1];
        return location;
    }
    
    // line, polygon and box build a geometry object to find the centroid
    var line = geo_search('georss:line');
    if (line && line.length > 0) {
        var coords = OpenLayers.String.trim(line).split(/\s+/);
        var p;
        var points = [];
        for (var i = 0; i < coords.length; i += 2) {
            p = new OpenLayers.Geometry.Point(
                parseFloat(coords[i + 1]),
                parseFloat(coords[i])
            );
            points.push(p);
        }
        geom = new OpenLayers.Geometry.LineString(points);
    }
    
    var polygon = geo_search('georss:polygon');
    if (polygon && polygon.length > 0) {
        var coords = OpenLayers.String.trim(polygon).split(/\s+/);
        var p;
        var points = [];
        for (var i = 0; i < coords.length; i += 2) {
            p = new OpenLayers.Geometry.Point(
                parseFloat(coords[i + 1]),
                parseFloat(coords[i])
            );
            points.push(p);
        }
        geom = new OpenLayers.Geometry.Polygon(
            [new OpenLayers.Geometry.LinearRing(points)]
        );
    }
    
    var box = geo_search('georss:box');
    if (box && box.length > 0) {
        var coords = OpenLayers.String.trim(box).split(/\s+/);
        var p;
        var points = [];
        points.push(new OpenLayers.Geometry.Point(  //lower-left
            parseFloat(coords[1]),
            parseFloat(coords[0])
        ));
        points.push(new OpenLayers.Geometry.Point(  //upper-left
            parseFloat(coords[1]),
            parseFloat(coords[2])
        ));
        points.push(new OpenLayers.Geometry.Point(  //upper-right
            parseFloat(coords[3]),
            parseFloat(coords[2])
        ));
        points.push(new OpenLayers.Geometry.Point(  //lower-right
            parseFloat(coords[3]),
            parseFloat(coords[0])
        ));
        points.push(new OpenLayers.Geometry.Point(  //repeat first point
            parseFloat(coords[1]),
            parseFloat(coords[0])
        ));
        geom = new OpenLayers.Geometry.Polygon(
            [new OpenLayers.Geometry.LinearRing(points)]
        );
    }
    
    if (geom) {
        var centroid = geom.getCentroid();
        location = centroid.y + ',' + centroid.x;
    }
    
    return location;
};


/**
Determines based on the current time, how old the Entry is, and generates
a string with how long ago the Entry was added.  ie. 5 minutes ago

Reference: http://stackoverflow.com/questions/3177836/how-to-format-time-since-xxx-e-g-4-minutes-ago-similar-to-stack-exchange-site
Future: http://momentjs.com/  instead for time formatting and parsing

@param {Integer} - the current time as an epoch timestamp
@return {String} - a formatted text string with how old the Entry is
*/
ACT.EntryClass.prototype.generate_since = function (current) {
    if (!current) {
        current = parseInt(new Date().getTime(), 10);
    }
    var diff = (current - this.timestamp) / 1000;
    // return just the date-time for more than 24 hours
    if (diff > 86400) {
        return this.entry_updated_display;
    }
    var interval = Math.floor(diff / 3600);
    if (interval >= 1) {
        return interval + ' hours ago';
    }
    interval = Math.floor(diff / 60);
    if (interval >= 1) {
        return interval + ' minutes ago';
    }
    return 'Less than a minute';
};


/**
Updates the Entry's since display value, called repetitively to show
it aging over time for the user. ie 5 mins ago, 10 mins ago, ...

@param {Integer} - the current time as an epoch timestamp
*/
ACT.EntryClass.prototype.update_since = function (current) {
    var $div = $('#entry' + this.seq);
    if (!$div) {
        throw new Error('Update Since: Entry not found');
    }
    var $updated_span = $div.find('.updated > span');
    if ($updated_span) {
        $updated_span.text(this.generate_since(current));
    }
};


/**
Generates an html representation of the Entry instance using a
Handlebars template so that it can be added to the page.
*/
ACT.EntryClass.prototype.generate_html = function () {
    // the seq number is managed by the feed and assigned to each
    // entry.  Its separate from the MASAS Entry ID because that ID
    // may appear multiple times, whereas this seq number is unique
    // to items added to the DOM
    if (!this.seq) {
        throw new Error('Template Error: Sequence number missing');
    }
    if (ACT.entry_template) {
        return ACT.entry_template(this);
    }
};


/**
Shows the details div for this Entry
*/
ACT.EntryClass.prototype.show = function () {
    var $div = $('#entry' + this.seq);
    if ($div) {
        var $details = $($div.find('.details')[0]);
        var $btn = $($div.find('.minMaxBtn i')[0]);
        if ($details.css('display') === 'none') {
            $details.css('display', '');
            $btn.removeClass('icon-plus-sign').addClass('icon-minus-sign');
        }
    }
};


/**
Hides the details div for this Entry
*/
ACT.EntryClass.prototype.hide = function () {
    var $div = $('#entry' + this.seq);
    if ($div) {
        var $details = $($div.find('.details')[0]);
        var $btn = $($div.find('.minMaxBtn i')[0]);
        if ($details.css('display') !== 'none') {
            $details.css('display', 'none');
            $btn.removeClass('icon-minus-sign').addClass('icon-plus-sign');
        }
    }
};


/**
Removes the Entry's html representation from the page, usually as
part of deleting the Entry because is aged out of the max number
of Entries stored.
*/
ACT.EntryClass.prototype.remove = function () {
    var $div = $('#entry' + this.seq);
    if (!$div) {
        throw new Error('Remove Error: Entry not found');
    }
    $div.empty().remove();
};
